package com.kdyzm.spring.security.oauth.study.resource.server.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author kdyzm
 */
@Configuration
@Import({com.kdyzm.spring.security.oauth.study.common.config.ApiWebMvcConfig.class})
public class Config {

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper();
    }
}
