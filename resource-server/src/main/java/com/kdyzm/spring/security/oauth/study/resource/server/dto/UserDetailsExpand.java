package com.kdyzm.spring.security.oauth.study.resource.server.dto;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * @author kdyzm
 */
@Data
public class UserDetailsExpand {

    private String username;

    //userId
    private Integer id;

    //电子邮箱
    private String email;

    //手机号
    private String mobile;

    private String fullname;
}
